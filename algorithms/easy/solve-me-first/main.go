package main

import "fmt"

func solveMeFirst(a uint32, b uint32) uint32 {
	return a + b
}

func main() {
	var a, b, res uint32
	fmt.Scanf("%v", &a)
	fmt.Scanf("%v", &b)
	res = solveMeFirst(a, b)
	fmt.Println(res)
}
