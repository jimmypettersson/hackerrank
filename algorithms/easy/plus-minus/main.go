package main

import "fmt"

func main() {
	var size int
	fmt.Scanf("%v", &size)

	var pos, neg, zero int
	for i := 0; i < size; i++ {
		var value int
		fmt.Scanf("%v", &value)

		if value > 0 {
			pos++
		} else if value < 0 {
			neg++
		} else {
			zero++
		}
	}

	fmt.Println(fmt.Sprintf("%1.6f", float64(pos)/float64(size)))
	fmt.Println(fmt.Sprintf("%1.6f", float64(neg)/float64(size)))
	fmt.Println(fmt.Sprintf("%1.6f", float64(zero)/float64(size)))
}
