package main

import "fmt"

func main() {
	var size int
	fmt.Scanf("%d", &size)

	var sum int
	for i := 0; i < size; i++ {
		var value int
		fmt.Scanf("%d", &value)
		sum += value
	}

	fmt.Println(sum)
}
