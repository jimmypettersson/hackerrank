package main

import (
	"fmt"
	"sort"
)

func main() {
	var t int
	fmt.Scanln(&t)

L:
	for ; t > 0; t-- {
		var n int
		fmt.Scanln(&n)

		var m = make([][]int, n)
		for i := 0; i < n; i++ {
			var bytes []byte
			fmt.Scanln(&bytes)

			var tmp = make([]int, n)
			for j := 0; j < n; j++ {
				tmp[j] = int(bytes[j])
			}

			sort.Ints(tmp)
			m[i] = tmp
		}

		for i := 0; i < n; i++ {
			for j := 0; j < n-1; j++ {
				if m[j][i] > m[j+1][i] {
					fmt.Println("NO")
					continue L
				}
			}
		}

		fmt.Println("YES")
	}
}
