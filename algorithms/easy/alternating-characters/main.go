package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var s string
		fmt.Scanln(&s)

		var d int
		for j := 1; j < len(s); j++ {
			if s[j] == s[j-1] {
				d++
			}
		}

		fmt.Println(d)
	}
}
