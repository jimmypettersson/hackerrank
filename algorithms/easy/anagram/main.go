package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var s string
		fmt.Scanln(&s)

		if len(s)%2 != 0 {
			fmt.Println(-1)
			continue
		}

		var s1, s2 = s[0 : len(s)/2], s[len(s)/2:]
		var counts = make([]int, 26)
		for j := 0; j < len(s1); j++ {
			counts[int(s1[j])%97]++
			counts[int(s2[j])%97]--
		}

		var n int
		for _, v := range counts {
			if v > 0 {
				n += v
			}
		}

		fmt.Println(n)
	}
}
