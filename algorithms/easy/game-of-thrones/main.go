package main

import "fmt"

func main() {
	var s string
	fmt.Scanln(&s)

	var counts = map[rune]int{}
	for _, c := range s {
		if _, ok := counts[c]; ok {
			counts[c]++
		} else {
			counts[c] = 1
		}
	}

	var hasOdd bool
	for _, v := range counts {
		if v%2 != 0 {
			if hasOdd {
				fmt.Println("NO")
				return
			}
			hasOdd = true
		}
	}

	fmt.Println("YES")
}
