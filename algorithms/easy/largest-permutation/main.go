package main

import (
	"fmt"
	"strings"
)

func main() {
	var n, k int
	fmt.Scanln(&n, &k)

	var A = make([]int, n)
	var B = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		A[i] = v
		B[v-1] = i
	}

	var swaps int
	var leftPos, rightPos = 0, len(B) - 1
	for swaps < k {
		if leftPos >= n || rightPos < 0 {
			break
		}

		var fromValue, toValue = A[leftPos], A[B[rightPos]]

		if fromValue == toValue {
			rightPos--
			continue
		} else if fromValue < toValue {
			A[leftPos], A[B[rightPos]] = A[B[rightPos]], A[leftPos]
			B[toValue-1], B[fromValue-1] = leftPos, B[rightPos]
			rightPos--
			swaps++
		}

		leftPos++
	}

	fmt.Println(strings.Trim(fmt.Sprint(A), "[]"))
}
