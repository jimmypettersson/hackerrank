package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var n int
		fmt.Scanf("%d", &n)
		fmt.Println(findDigits(n))
	}
}

func findDigits(n int) int {
	s := strings.Split(strconv.Itoa(n), "")

	var divisible int
	for _, e := range s {
		j, _ := strconv.Atoi(e)
		if j > 0 && n%j == 0 {
			divisible++
		}

	}

	return divisible
}
