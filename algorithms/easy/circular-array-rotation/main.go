package main

import "fmt"

func main() {
	var n, k, q int
	fmt.Scanln(&n, &k, &q)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[(i+k)%len(arr)] = v
	}

	for i := 0; i < q; i++ {
		var v int
		fmt.Scan(&v)
		fmt.Println(arr[v])
	}
}
