package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

const size = 10000

func main() {
	b, _ := ioutil.ReadAll(os.Stdin)
	lines := strings.Split(string(b), "\n")
	var counts = make([]int, size)

	n, _ := strconv.Atoi(lines[0])
	aNums := strings.Fields(lines[1])
	for i := 0; i < n; i++ {
		v, _ := strconv.Atoi(aNums[i])
		counts[v-1]++
	}

	m, _ := strconv.Atoi(lines[2])
	bNums := strings.Fields(lines[3])
	for i := 0; i < m; i++ {
		v, _ := strconv.Atoi(bNums[i])
		counts[v-1]--
	}

	for i := 0; i < size; i++ {
		if counts[i] < 0 {
			fmt.Print(i+1, " ")
		}
	}
	fmt.Println()
}
