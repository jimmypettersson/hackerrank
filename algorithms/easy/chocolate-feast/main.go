package main

import "fmt"

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var n, c, m int
		fmt.Scanf("%d %d %d", &n, &c, &m)
		fmt.Println(purchaseChocolate(n, c, m))
	}
}

func purchaseChocolate(cash, cost, discountThreshold int) int {
	var bought, wrappers int

	for {
		w := cash / cost
		if w == 0 {
			break
		}

		cash -= w * cost
		bought += w
		wrappers += w
		for wrappers >= discountThreshold {
			bought++
			wrappers -= (discountThreshold - 1)
		}
	}

	return bought
}
