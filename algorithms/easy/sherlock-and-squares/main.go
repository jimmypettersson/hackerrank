package main

import (
	"fmt"
	"math"
)

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var low, high int
		fmt.Scanf("%d %d", &low, &high)
		fmt.Println(calcSquares(low, high))
	}
}

func calcSquares(low, high int) int {
	return int(math.Ceil((math.Floor(math.Sqrt(float64(high))) + 1)) - math.Sqrt(float64(low)))
}
