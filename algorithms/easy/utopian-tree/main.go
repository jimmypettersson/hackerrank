package main

import "fmt"

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var n int
		fmt.Scanf("%d", &n)
		fmt.Println(calculateHeight(n))
	}
}

func calculateHeight(n int) int {
	var h = 1

	for c := 0; c < n; c++ {
		if c%2 == 0 {
			h *= 2
		} else {
			h++
		}
	}

	return h
}
