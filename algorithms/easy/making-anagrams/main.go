package main

import (
	"fmt"
	"math"
)

func main() {
	var s1, s2 string
	fmt.Scanln(&s1)
	fmt.Scanln(&s2)

	var counts = make([]int, 26)
	for _, c := range s1 {
		counts[int(c)%97]++
	}

	for _, c := range s2 {
		counts[int(c)%97]--
	}

	var n int
	for _, v := range counts {
		n += int(math.Abs(float64(v)))
	}

	fmt.Println(n)
}
