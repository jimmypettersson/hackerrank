package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[i] = v
	}

	var e = arr[len(arr)-1]
	var inserted bool
	for i := len(arr) - 1; i > 0; i-- {
		if arr[i-1] > e {
			arr[i] = arr[i-1]
			print(arr)
		} else {
			arr[i] = e
			inserted = true
			break
		}
	}

	if !inserted {
		arr[0] = e
	}

	print(arr)
}

func print(arr []int) {
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	fmt.Println()
}
