package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n int
		fmt.Scanln(&n)

		var acc = make([]int, n)
		for i := 0; i < n; i++ {
			var n int
			fmt.Scan(&n)
			if i == 0 {
				acc[i] = n
			} else {
				acc[i] = n + acc[i-1]
			}
		}

		if n == 1 {
			fmt.Println("YES")
			continue
		}

		var exists bool
		for i := 1; i < n-1; i++ {
			if acc[n-1]-acc[i] == acc[i-1] {
				exists = true
				break
			}
		}

		if exists {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
