package main

import (
	"fmt"
	"math"
	"strconv"
)

func main() {
	var p, q int
	fmt.Scanln(&p)
	fmt.Scanln(&q)

	var kaprekarNumbers = []int{}
	for i := p; i <= q; i++ {
		if isKaprekar(i) {
			kaprekarNumbers = append(kaprekarNumbers, i)
		}
	}

	for i, v := range kaprekarNumbers {
		fmt.Print(v, " ")
		if i == len(kaprekarNumbers)-1 {
			fmt.Println()
		}
	}

	if len(kaprekarNumbers) == 0 {
		fmt.Println("INVALID RANGE")
	}
}

func isKaprekar(n int) bool {
	squared := int64(math.Pow(float64(n), 2))
	if squared == int64(n) {
		return true
	}

	var s = strconv.FormatInt(squared, 10)
	for i := 0; i < len(s)-1; i++ {
		ls := s[:i+1]
		rs := s[i+1:]
		if len(ls) == len(rs) || len(ls) == len(rs)-1 {
			l, _ := strconv.Atoi(ls)
			r, _ := strconv.Atoi(rs)
			if l > 0 && r > 0 && l+r == n {
				return true
			}
		}
	}

	return false
}
