package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

type point struct {
	row, col int
}

func main() {
	var n int
	fmt.Scanln(&n)

	var m = make([][]int, n)
	for i := 0; i < n; i++ {
		m[i] = make([]int, n)

		var s string
		fmt.Scanln(&s)

		for j, e := range strings.Split(s, "") {
			v, _ := strconv.Atoi(e)
			m[i][j] = v
		}
	}

	cavities := findCavities(m)
	printCavities(m, cavities)
}

func findCavities(m [][]int) []point {
	var cavities = []point{}

	for row := 1; row < len(m)-1; row++ {
		for col := 1; col < len(m[0])-1; col++ {
			la := largestAdjacent(m, row, col)
			if la < m[row][col] {
				cavities = append(cavities, point{row, col})
			}
		}
	}

	return cavities
}

func largestAdjacent(m [][]int, row, col int) int {
	var max = m[row-1][col]
	for _, v := range []int{m[row][col-1], m[row][col+1], m[row+1][col]} {
		if v > max {
			max = v
		}
	}

	return max
}

func printCavities(m [][]int, cavities []point) {
	for _, p := range cavities {
		m[p.row][p.col] = 0
	}

	var buf bytes.Buffer
	for row := 0; row < len(m); row++ {
		for col := 0; col < len(m); col++ {
			if m[row][col] == 0 {
				buf.WriteString("X")
			} else {
				buf.WriteString(strconv.Itoa(m[row][col]))
			}
		}
		buf.WriteString("\n")
	}

	fmt.Println(buf.String())
}
