package main

import (
	"fmt"
	"sort"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n, k int
		fmt.Scanln(&n, &k)

		var A, B = make([]int, n), make([]int, n)
		for i := 0; i < n; i++ {
			var v int
			fmt.Scan(&v)
			A[i] = v
		}
		for i := 0; i < n; i++ {
			var v int
			fmt.Scan(&v)
			B[i] = v
		}

		sort.Ints(A)
		sort.Sort(sort.Reverse(sort.IntSlice(B)))

		var pairs int
	L:
		for i := 0; i < n; i++ {
			if A[i]+B[i] < k {
				break L
			}

			pairs++
		}

		if pairs == n {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
