package main

import "fmt"

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var N, K, inTime int
		fmt.Scanf("%d %d", &N, &K)

		for j := 0; j < N; j++ {
			var arrivalTime int
			fmt.Scanf("%d", &arrivalTime)
			if arrivalTime <= 0 {
				inTime++
			}
		}

		if inTime < K {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
