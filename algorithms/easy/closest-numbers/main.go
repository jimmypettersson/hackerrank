package main

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

func main() {
	var n int
	fmt.Scanln(&n)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[i] = v
	}

	sort.Ints(arr)

	var min, minPairs = math.MaxUint32, []int{}
	for i := 0; i < len(arr)-1; i++ {
		var diff = arr[i+1] - arr[i]
		if diff < min {
			min = diff
			minPairs = []int{arr[i], arr[i+1]}
		} else if diff == min {
			minPairs = append(minPairs, arr[i], arr[i+1])
		}
	}

	fmt.Println(strings.Trim(fmt.Sprint(minPairs), "[]"))
}
