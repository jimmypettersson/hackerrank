package main

import (
	"fmt"
	"strings"
)

func main() {
	var T int
	fmt.Scanf("%d", &T)

	for i := 0; i < T; i++ {
		var N int
		fmt.Scanf("%d", &N)
		fmt.Println(findBiggestDecent(N))
	}
}

func findBiggestDecent(n int) string {
	if n%3 == 0 {
		return strings.Repeat("5", n)
	}

	for i := 0; i < n; i = i + 5 {
		if (n-i)%3 == 0 && i%5 == 0 {
			return strings.Repeat("5", n-i) + strings.Repeat("3", i)
		}
	}

	if n%5 == 0 {
		return strings.Repeat("3", n)
	}

	return "-1"
}
