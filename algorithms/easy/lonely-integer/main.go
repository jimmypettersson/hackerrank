package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var x int
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		x ^= v
	}

	fmt.Println(x)
}
