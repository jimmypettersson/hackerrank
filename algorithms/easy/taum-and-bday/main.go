package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var b, w, x, y, z int
		fmt.Scanln(&b, &w)
		fmt.Scanln(&x, &y, &z)
		fmt.Println(b*min(x, y+z) + w*min(y, x+z))
	}
}

func min(x, y int) int {
	if x < y {
		return x
	}

	return y
}
