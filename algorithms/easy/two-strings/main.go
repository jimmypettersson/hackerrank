package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var a, b []byte
		fmt.Scanln(&a)
		fmt.Scanln(&b)

		var ac, bc = make([]int, 26), make([]int, 26)

		for _, v := range a {
			ac[int(v)%97]++
		}
		for _, v := range b {
			bc[int(v)%97]++
		}

		var hasSS bool
		for j := range ac {
			if ac[j] > 0 && bc[j] > 0 {
				hasSS = true
				break
			}
		}

		if hasSS {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
