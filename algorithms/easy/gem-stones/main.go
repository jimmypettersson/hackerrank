package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var allCounts = make([]int, 26)
	for i := 0; i < n; i++ {
		var b []byte
		fmt.Scanln(&b)

		var counts = map[int]struct{}{}
		for _, v := range b {
			counts[int(v)%97] = struct{}{}
		}

		for k := range counts {
			allCounts[k]++
		}
	}

	var g int
	for _, v := range allCounts {
		if v == n {
			g++
		}
	}

	fmt.Println(g)
}
