package main

import "fmt"

func main() {
	var n, m int
	fmt.Scanf("%d %d", &n, &m)

	var people = make([][]byte, n)

	for i := 0; i < n; i++ {
		var b []byte
		fmt.Scanln(&b)
		for j := 0; j < len(b); j++ {
			b[j] = b[j] % 48
		}
		people[i] = b
	}

	var maxScore int
	var maxScoreTeams int
	for i := 0; i < len(people); i++ {
		for j := i + 1; j < len(people); j++ {
			var score int
			for k := 0; k < m; k++ {
				score += int(people[i][k] | people[j][k])
			}

			if score == maxScore {
				maxScoreTeams++
			}

			if score > maxScore {
				maxScore = score
				maxScoreTeams = 1
			}
		}
	}

	fmt.Println(maxScore)
	fmt.Println(maxScoreTeams)
}
