package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n uint32
		fmt.Scanln(&n)
		fmt.Println(n ^ (1<<32 - 1))
	}
}
