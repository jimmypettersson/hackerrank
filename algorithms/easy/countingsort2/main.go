package main

import (
	"fmt"
	"strings"
)

const maxVal = 100

func main() {
	var n int
	fmt.Scanln(&n)

	var counts = make([]int, maxVal)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		counts[v]++
	}

	fmt.Println(strings.Trim(fmt.Sprint(countingSort(counts, n)), "[]"))
}

func countingSort(counts []int, maxLen int) []int {
	var total, out = 0, make([]int, maxLen)
	for i := 0; i < len(counts); i++ {
		for j := 0; j < counts[i]; j++ {
			out[total] = i
			total++
		}
	}

	return out
}
