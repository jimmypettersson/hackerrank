package main

import (
	"bytes"
	"fmt"
)

func main() {
	var n, k int
	var s string

	fmt.Scanf("%d", &n)
	fmt.Scanf("%s", &s)
	fmt.Scanf("%d", &k)
	fmt.Println(encode(n, k, []byte(s)))
}

func encode(n, k int, s []byte) string {
	var buf bytes.Buffer

	for i := 0; i < n; i++ {
		// uppercase
		if s[i] >= 65 && s[i] <= 90 {
			j := int(s[i]) - 65
			d := (j + k) % 26
			buf.WriteByte(byte(d + 65))
			continue
		}

		// lowercase
		if s[i] >= 97 && s[i] <= 122 {
			j := int(s[i]) - 97
			d := (j + k) % 26
			buf.WriteByte(byte(d + 97))
			continue
		}

		buf.WriteByte(s[i])
	}

	return buf.String()
}
