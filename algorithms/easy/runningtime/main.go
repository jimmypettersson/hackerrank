package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[i] = v
	}

	var shifts int
	for i := 1; i < len(arr); i++ {
		shifts += insertOne(arr[i], arr[0:i+1])
	}

	fmt.Println(shifts)
}

func insertOne(e int, arr []int) int {
	var n int
	for i := len(arr) - 1; i > 0; i-- {
		if arr[i-1] > e {
			n++
			arr[i] = arr[i-1]
		} else {
			arr[i] = e
			return n
		}
	}

	arr[0] = e
	return n
}
