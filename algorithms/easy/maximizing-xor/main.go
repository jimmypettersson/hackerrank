package main

import "fmt"

func main() {
	var l, r int
	fmt.Scanln(&l)
	fmt.Scanln(&r)

	var max int
	for i := l; i <= r; i++ {
		for j := i; j <= r; j++ {
			var x = i ^ j
			if x > max {
				max = x
			}
		}
	}

	fmt.Println(max)
}
