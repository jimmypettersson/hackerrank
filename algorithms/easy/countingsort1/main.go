package main

import (
	"fmt"
	"strings"
)

func main() {
	var n int
	fmt.Scanln(&n)

	var counts = make([]int, 100)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		counts[v]++
	}

	fmt.Println(strings.Trim(fmt.Sprint(counts), "[]"))
}
