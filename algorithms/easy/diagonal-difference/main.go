package main

import "fmt"

func main() {
	var size int
	fmt.Scanf("%v", &size)

	var primarySum, secondarySum int
	for col := 0; col < size; col++ {
		for row := 0; row < size; row++ {
			var value int
			fmt.Scanf("%v", &value)

			if col == row {
				primarySum += value
			}

			if col+row == size-1 {
				secondarySum += value
			}
		}
	}

	diff := primarySum - secondarySum
	if diff >= 0 {
		fmt.Println(diff)
	} else {
		fmt.Println(-diff)
	}
}
