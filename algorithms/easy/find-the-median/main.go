package main

import "fmt"

const offset = 1e4

func main() {
	var n int
	fmt.Scanln(&n)

	var counts = make([]int, 1+2*offset)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		counts[v+offset]++
	}

	for i := 1; i < len(counts); i++ {
		counts[i] += counts[i-1]
		if counts[i] >= (n/2)+1 {
			fmt.Println(i - offset)
			break
		}
	}
}
