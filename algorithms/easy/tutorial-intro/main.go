package main

import (
	"fmt"
)

func main() {
	var v, n int
	fmt.Scanln(&v)
	fmt.Scanln(&n)

	for i := 0; i < n; i++ {
		var a int
		fmt.Scanf("%d", &a)
		if a == v {
			fmt.Println(i)
			break
		}
	}
}
