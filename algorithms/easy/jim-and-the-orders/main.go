package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

type order struct {
	id       int
	doneTime int
}

func (o order) String() string {
	return strconv.Itoa(o.id)
}

type byDoneTime []order

func (o byDoneTime) Len() int {
	return len(o)
}

func (o byDoneTime) Swap(i, j int) {
	o[i], o[j] = o[j], o[i]
}

func (o byDoneTime) Less(i, j int) bool {
	if o[i].doneTime == o[j].doneTime {
		return o[i].id < o[j].id
	}

	return o[i].doneTime < o[j].doneTime
}

func main() {
	var n int
	fmt.Scanln(&n)

	var orders = make([]order, n)
	for i := 0; i < n; i++ {
		var t, d int
		fmt.Scanln(&t, &d)
		orders[i] = order{i + 1, t + d}
	}

	sort.Sort(byDoneTime(orders))
	fmt.Println(strings.Trim(fmt.Sprint(orders), "[]"))
}
