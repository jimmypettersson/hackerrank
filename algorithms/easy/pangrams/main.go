package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	b, _ := ioutil.ReadAll(bufio.NewReader(os.Stdin))
	s := strings.ToLower(string(b))
	s = strings.Replace(s, " ", "", -1)

	var found = map[rune]struct{}{}
	for _, c := range s {
		found[c] = struct{}{}
		if len(found) == 26 {
			fmt.Println("pangram")
			return
		}
	}

	fmt.Println("not pangram")
}
