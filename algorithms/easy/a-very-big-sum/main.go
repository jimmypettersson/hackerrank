package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%v", &n)

	var sum int64
	for i := 0; i < n; i++ {
		var j int64
		fmt.Scanf("%v", &j)
		sum += j
	}

	fmt.Println(sum)
}
