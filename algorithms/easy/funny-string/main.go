package main

import (
	"fmt"
	"math"
)

func main() {
	var t int
	fmt.Scanf("%d", &t)

	for i := 0; i < t; i++ {
		var b []byte
		fmt.Scanln(&b)

		var funny = true
		for j := 1; j < len(b); j++ {
			var lhs = int(b[j]) - int(b[j-1])
			var rhs = int(b[len(b)-j]) - int(b[len(b)-1-j])
			if math.Abs(float64(lhs)) != math.Abs(float64(rhs)) {
				funny = false
				break
			}
		}

		if funny {
			fmt.Println("Funny")
		} else {
			fmt.Println("Not Funny")
		}
	}
}
