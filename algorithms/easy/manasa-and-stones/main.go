package main

import (
	"fmt"
	"sort"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for i := 0; i < t; i++ {
		var n, a, b int
		fmt.Scanln(&n)
		fmt.Scanln(&a)
		fmt.Scanln(&b)

		var stones = make([]int, n)
		for j := 1; j < len(stones); j++ {
			stones[j] = a
		}

		var solutions = []int{sum(stones)}

		for j := 1; j < n; j++ {
			stones[j] = b
			solutions = append(solutions, sum(stones))
		}

		sort.Ints(solutions)
		var uniqueSolutions = []int{solutions[0]}
		for k := 1; k < len(solutions); k++ {
			if solutions[k] != uniqueSolutions[len(uniqueSolutions)-1] {
				uniqueSolutions = append(uniqueSolutions, solutions[k])
			}
		}

		for _, v := range uniqueSolutions {
			fmt.Print(v, " ")
		}
		fmt.Println()
	}
}

func sum(numbers []int) int {
	var sum int
	for _, v := range numbers {
		sum += v
	}
	return sum
}
