package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	fmt.Scanf("%d", &n)

	var sticks = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scanf("%d", &v)
		sticks[i] = v
	}

	for {
		sort.Ints(sticks)
		min := sticks[0]

		var cutIndex = 0
		for i := 0; i < len(sticks); i++ {
			sticks[i] -= min
			if sticks[i] <= 0 {
				cutIndex++
			}
		}

		fmt.Println(len(sticks))
		sticks = sticks[cutIndex:]
		if len(sticks) == 0 {
			break
		}
	}
}
