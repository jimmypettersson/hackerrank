package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var b, _ = ioutil.ReadAll(os.Stdin)
	var lines = strings.Split(string(b), "\n")

	var nk = strings.Fields(lines[0])
	var n, _ = strconv.Atoi(nk[0])
	var k, _ = strconv.Atoi(nk[1])

	var arr = make([]int, n)
	var numbers = strings.Fields(lines[1])
	for i, n := range numbers {
		var nn, _ = strconv.Atoi(n)
		arr[i] = nn
	}

	sort.Ints(arr)

	var sum int
	for i := 0; i < n; i++ {
		sum += arr[i]
		if sum >= k {
			fmt.Println(i)
			break
		}
	}
}
