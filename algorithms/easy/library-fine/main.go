package main

import (
	"fmt"
	"time"
)

func main() {
	var aDay, aMonth, aYear int
	var eDay, eMonth, eYear int

	fmt.Scanf("%d %d %d", &aDay, &aMonth, &aYear)
	fmt.Scanf("%d %d %d", &eDay, &eMonth, &eYear)

	actual, _ := time.Parse("2 1 2006", fmt.Sprintf("%d %d %d", aDay, aMonth, aYear))
	expected, _ := time.Parse("2 1 2006", fmt.Sprintf("%d %d %d", eDay, eMonth, eYear))

	if actual.Before(expected) {
		fmt.Println(0)
		return
	}

	if actual.Year() > expected.Year() {
		fmt.Println(10000)
		return
	}

	if actual.Month() > expected.Month() {
		fmt.Println(500 * (int(actual.Month()) - int(expected.Month())))
		return
	}

	if actual.Day() > expected.Day() {
		fmt.Println(15 * (actual.Day() - expected.Day()))
		return
	}

	fmt.Println(0)
}
