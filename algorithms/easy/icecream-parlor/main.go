package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var m, n int
		fmt.Scanln(&m)
		fmt.Scanln(&n)

		var arr = make([]int, n)
		for i := 0; i < n; i++ {
			var v int
			fmt.Scan(&v)
			arr[i] = v
		}

	L:
		for i := 0; i < n; i++ {
			for j := i + 1; j < n; j++ {
				if arr[i]+arr[j] == m {
					fmt.Println(i+1, j+1)
					break L
				}
			}
		}
	}
}
