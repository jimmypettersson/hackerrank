package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[i] = v
	}

	for i := 1; i < len(arr); i++ {
		insertOne(arr[i], arr[0:i+1])
		print(arr)
	}
}

func insertOne(e int, arr []int) {
	for i := len(arr) - 1; i > 0; i-- {
		if arr[i-1] > e {
			arr[i] = arr[i-1]
		} else {
			arr[i] = e
			return
		}
	}

	arr[0] = e
}

func print(arr []int) {
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	fmt.Println()
}
