package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var s []byte
		fmt.Scanln(&s)

		if isPalindrome2(s, -1) {
			fmt.Println(-1)
			continue
		}

	M:
		for i := 0; i < len(s); i++ {
			if isPalindrome2(s, i) {
				fmt.Println(i)
				break M
			}
		}
	}
}

func isPalindrome2(b []byte, skipPos int) bool {
	for i, j := 0, len(b)-1; i < j; i, j = i+1, j-1 {
		if i == skipPos {
			i++
		}
		if j == skipPos {
			j--
		}

		if b[i] != b[j] {
			return false
		}
	}

	return true
}
