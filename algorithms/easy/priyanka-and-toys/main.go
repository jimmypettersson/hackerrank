package main

import (
	"fmt"
	"sort"
)

func main() {
	var n int
	fmt.Scanln(&n)

	var W = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		W[i] = v
	}

	sort.Ints(W)

	var ans int
	var segmentLength = 1
	var segmentIndex = 0
	for i := 0; i < n-1; i++ {
		if W[i+1]-W[segmentIndex] < 5 {
			segmentLength++
		} else {
			ans++
			segmentLength = 1
			segmentIndex = i + 1
		}
	}

	if segmentLength > 0 {
		ans++
	}

	fmt.Println(ans)
}
