package main

import (
	"fmt"
	"time"
)

func main() {
	var input string
	fmt.Scanf("%s", &input)

	t, _ := time.Parse("3:04:05PM", input)
	fmt.Println(t.Format("15:04:05"))
}
