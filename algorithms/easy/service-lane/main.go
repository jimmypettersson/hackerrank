package main

import "fmt"

func main() {
	var n, t int
	fmt.Scanf("%d %d", &n, &t)

	var widths = make([]int, n)
	for i := 0; i < n; i++ {
		var w int
		fmt.Scanf("%d", &w)
		widths[i] = w
	}

	for k := 0; k < t; k++ {
		var i, j int
		fmt.Scanf("%d %d", &i, &j)
		fmt.Println(calculateVehicleType(i, j, widths))
	}
}

func calculateVehicleType(i, j int, widths []int) int {
	return min(widths[i : j+1])
}

func min(n []int) int {
	var min = n[0]
	for _, e := range n {
		if e < min {
			min = e
		}
	}

	return min
}
