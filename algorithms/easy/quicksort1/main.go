package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var left, equal, right = []int{}, []int{}, []int{}
	var p int
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		if i == 0 {
			p = v
		}

		if v < p {
			left = append(left, v)
		} else if v > p {
			right = append(right, v)
		} else {
			equal = append(equal, v)
		}
	}

	print(append(left, append(equal, right...)...))
}

func print(arr []int) {
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	fmt.Println()
}
