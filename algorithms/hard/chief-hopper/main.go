package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var arr = make([]int64, n)
	for i := 0; i < n; i++ {
		var v int64
		fmt.Scan(&v)
		arr[i] = v
	}

	var e int64
	for i := n - 1; i >= 0; i-- {
		var s = e + arr[i]
		if s%2 == 0 {
			e = s / 2
		} else {
			e = s/2 + 1
		}
	}

	fmt.Println(e)
}
