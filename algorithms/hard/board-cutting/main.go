package main

import (
	"fmt"
	"sort"
)

const modN = 1e9 + 7

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var m, n int
		fmt.Scanln(&m, &n)

		var xcosts, ycosts = make([]int, m), make([]int, n)
		for i := 0; i < m-1; i++ {
			var v int
			fmt.Scan(&v)
			xcosts[i] = v
		}
		for i := 0; i < n-1; i++ {
			var v int
			fmt.Scan(&v)
			ycosts[i] = v
		}

		sort.Sort(sort.Reverse(sort.IntSlice(xcosts)))
		sort.Sort(sort.Reverse(sort.IntSlice(ycosts)))

		var xCuts, yCuts = 1, 1
		var cost, i, j int

		for i < m && j < n {
			if xcosts[i] >= ycosts[j] {
				cost = (cost + yCuts*xcosts[i]) % modN
				xCuts++
				i++
			} else {
				cost = (cost + xCuts*ycosts[j]) % modN
				yCuts++
				j++
			}
		}

		for ; i < m; i++ {
			cost = (cost + yCuts*xcosts[i]) % modN
		}

		for ; j < n; j++ {
			cost = (cost + xCuts*ycosts[j]) % modN
		}

		fmt.Println(cost % modN)
	}
}
