package main

import "fmt"

func main() {
	var m, n, r int
	fmt.Scanln(&m, &n, &r)

	var matrix = make([][]int, m)
	for i := 0; i < m; i++ {
		matrix[i] = make([]int, n)
		for j := 0; j < n; j++ {
			var v int
			fmt.Scan(&v)
			matrix[i][j] = v
		}
	}

	var nLayers = int(min(m, n) / 2)
	for j := 0; j < nLayers; j++ {
		var l = layer(j, matrix)
		var rr = r % len(l)
		for i := 0; i < rr; i++ {
			insert(j, append(l[1:], l[0]), matrix)
			l = layer(j, matrix)
		}
	}

	pretty(matrix)
}

func layer(l int, m [][]int) []int {
	var layer = []int{}
	for r := l; r < len(m)-l; r++ {
		if r == l {
			layer = append(layer, m[r][l:len(m[0])-l]...)
		} else if r == len(m)-l-1 {
			layer = append(layer, reverse(m[r][l:len(m[0])-l])...)
		} else {
			layer = append(layer, m[r][len(m[0])-l-1])
		}
	}

	for r := len(m) - l - 2; r >= l+1; r-- {
		layer = append(layer, m[r][l])
	}

	return layer
}

func insert(l int, ll []int, m [][]int) {
	var idx int
	for r := l; r < len(m)-l; r++ {
		if r == l {
			for i := l; i < len(m[0])-l; i++ {
				m[r][i] = ll[idx]
				idx++
			}
		} else if r == len(m)-l-1 {
			for i := len(m[0]) - l - 1; i >= l; i-- {
				m[r][i] = ll[idx]
				idx++
			}
		} else {
			m[r][len(m[0])-l-1] = ll[idx]
			idx++
		}
	}

	for r := len(m) - l - 2; r >= l+1; r-- {
		m[r][l] = ll[idx]
		idx++
	}
}

func pretty(m [][]int) {
	for r := 0; r < len(m); r++ {
		for c := 0; c < len(m[0]); c++ {
			fmt.Print(m[r][c], " ")
		}
		fmt.Println()
	}
	fmt.Println()
}

func reverse(s []int) []int {
	var n = make([]int, len(s))
	copy(n, s)
	for i, j := 0, len(n)-1; i < j; i, j = i+1, j-1 {
		n[i], n[j] = n[j], n[i]
	}
	return n
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}
