package main

import "fmt"

const modP = 1e9 + 7

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n int
		fmt.Scanln(&n)

		var result uint64
		for i := 0; i < n; i++ {
			var v uint64
			fmt.Scan(&v)
			result = (result | v) % modP
		}

		for i := 0; i < n-1; i++ {
			result = (result * 2) % modP
		}

		fmt.Println(result)
	}
}
