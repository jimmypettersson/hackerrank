package main

import (
	"container/heap"
	"fmt"
	"math"
)

type Item struct {
	value    *node
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQueue) update(item *Item, priority int) {
	item.priority = priority
	heap.Fix(pq, item.index)
}

type node struct {
	id       int
	distance int //distance to source
	adjacent map[*node]int
}

func (n *node) addEdge(other *node, r int) {
	// Only add same edge if its shorter
	if rr, ok := n.adjacent[other]; ok {
		if r < rr {
			n.adjacent[other] = r
			other.adjacent[n] = r
		}
	} else {
		n.adjacent[other] = r
		other.adjacent[n] = r
	}
}

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n, m, s int
		fmt.Scanln(&n, &m)

		var graph []*node
		for i := 0; i < n; i++ {
			graph = append(graph, &node{id: i, adjacent: map[*node]int{}})
		}

		for i := 0; i < m; i++ {
			var x, y, r int
			fmt.Scanln(&x, &y, &r)

			graph[x-1].addEdge(graph[y-1], r)
		}

		fmt.Scanln(&s)
		printDistances(dijkstra(graph, graph[s-1]))
	}
}

func dijkstra(graph []*node, source *node) []int {
	var pq = make(PriorityQueue, len(graph))
	var items = make([]*Item, len(graph))
	var dist = make([]int, len(graph))

	for _, v := range graph {
		dist[v.id] = int(math.MaxInt32)
		pq[v.id] = &Item{v, dist[v.id], v.id}
		items[v.id] = pq[v.id]
	}

	dist[source.id] = 0
	pq[source.id].priority = 0
	heap.Init(&pq)

	for pq.Len() > 0 {
		item := heap.Pop(&pq).(*Item)
		var u = item.value
		for v, r := range u.adjacent {
			var alt = dist[u.id] + r
			if alt < dist[v.id] {
				dist[v.id] = alt
				pq.update(items[v.id], alt)
			}
		}
	}

	return dist
}

func printDistances(dist []int) {
	for _, v := range dist {
		switch {
		case v == 0:
			continue
		case v == int(math.MaxInt32):
			fmt.Print(-1)
		default:
			fmt.Print(v)
		}
		fmt.Print(" ")
	}
	fmt.Println()
}
