package main

import (
	"fmt"
	"math"
)

func main() {
	var n, m, q int
	fmt.Scanln(&n, &m)

	var dist = make([][]int, n)
	for i := 0; i < n; i++ {
		dist[i] = make([]int, n)
		for j := 0; j < n; j++ {
			dist[i][j] = int(math.MaxInt32)
			if i == j {
				dist[i][j] = 0
			}
		}
	}

	for i := 0; i < m; i++ {
		var x, y, r int
		fmt.Scanln(&x, &y, &r)

		dist[x-1][y-1] = r
	}

	for k := 0; k < n; k++ {
		for i := 0; i < n; i++ {
			for j := 0; j < n; j++ {
				var d = dist[i][k] + dist[k][j]
				if d < dist[i][j] {
					dist[i][j] = d
				}
			}
		}
	}

	fmt.Scanln(&q)
	var inf = int(math.MaxInt32)
	for i := 0; i < q; i++ {
		var x, y int
		fmt.Scanln(&x, &y)
		var v = dist[x-1][y-1]
		if v == inf {
			fmt.Println(-1)
		} else {
			fmt.Println(v)
		}
	}
}
