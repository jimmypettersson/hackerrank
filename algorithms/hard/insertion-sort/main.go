package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	b, _ := ioutil.ReadAll(os.Stdin)
	fields := bytes.Split(b, []byte("\n"))
	t, _ := strconv.Atoi(string(fields[0]))

	for i := 0; i < t; i++ {
		n, _ := strconv.Atoi(string(fields[2*i+1]))
		nbrs := strings.Fields(string(fields[2*i+2]))
		var arr = make([]int, n)
		for i, v := range nbrs {
			val, _ := strconv.Atoi(v)
			arr[i] = val
		}

		_, inversions := mergeSort(arr)
		fmt.Println(inversions)
	}
}

func merge(l, r []int) ([]int, int) {
	var ret = make([]int, len(l)+len(r))
	var i, j, inversions int

	for i < len(l) && j < len(r) {
		if l[i] <= r[j] {
			inversions += j
			ret[i+j] = l[i]
			i++
		} else {
			ret[i+j] = r[j]
			j++
		}
	}

	inversions += j * (len(l) - i)

	for i < len(l) {
		ret[i+j] = l[i]
		i++
	}
	for j < len(r) {
		ret[i+j] = r[j]
		j++
	}

	return ret, inversions
}

func mergeSort(s []int) ([]int, int) {
	if len(s) <= 1 {
		return s, 0
	}

	n := len(s) / 2
	l, il := mergeSort(s[:n])
	r, ir := mergeSort(s[n:])
	ss, i := merge(l, r)
	return ss, i + il + ir
}
