package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var b, _ = ioutil.ReadAll(os.Stdin)
	var lines = strings.Split(string(b), "\n")

	var n, _ = strconv.Atoi(lines[0])
	var p, _ = strconv.Atoi(strings.Fields(lines[2])[0])
	var q, _ = strconv.Atoi(strings.Fields(lines[2])[1])

	var arr = make([]int, n)
	var numbers = strings.Fields(lines[1])

	for i, n := range numbers {
		var nn, _ = strconv.Atoi(n)
		arr[i] = nn
	}

	sort.Ints(arr)

	var maxx, ans int
	if p < arr[0] {
		maxx = abs(arr[0] - p)
		ans = p
	}

	for i := 0; i < n-1; i++ {
		var v = (arr[i+1] - arr[i]) / 2
		var pos = arr[i] + v

		if pos > q {
			if v-(pos-q) > maxx {
				maxx = v - (pos - q)
				ans = q
			}

			break
		}

		if v > maxx && pos <= q && pos >= p {
			maxx = v
			ans = pos
		}
	}
	if q > arr[len(arr)-1] {
		if abs(arr[len(arr)-1]-q) > maxx {
			maxx = abs(arr[len(arr)-1] - q)
			ans = q
		}
	}

	fmt.Println(ans)
}

func abs(a int) int {
	if a < 0 {
		return -a
	}

	return a
}

func min(a, b int) int {
	if a < b {
		return a
	}

	return b
}
