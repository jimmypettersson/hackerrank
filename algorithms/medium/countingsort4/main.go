package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
)

const maxVal = 100

func main() {
	var dash, space = []byte("-"), []byte(" ")
	var n int
	fmt.Scanln(&n)

	var input = make([]int, n)
	var counts = make([]int, maxVal)
	var stringz = make([][]byte, n)

	b, _ := ioutil.ReadAll(os.Stdin)
	var in = bytes.Split(b, []byte("\n"))

	for i, v := range in[:n] {
		var splitIndex = bytes.Index(v, space)

		var c int
		if splitIndex == 1 {
			c = int(v[0] % 48)
		} else {
			c = 10*int(v[0]%48) + int(v[1]%48)
		}

		counts[c]++
		input[i] = c
		if i >= n/2 {
			stringz[i] = v[splitIndex+1:]
		} else {
			stringz[i] = dash
		}
	}

	var out = make([][]byte, n)
	acc := accumulateCounts(counts)
	for i := 0; i < len(stringz); i++ {
		out[acc[input[i]]-counts[input[i]]] = stringz[i]
		counts[input[i]]--
	}

	var buf bytes.Buffer
	for _, v := range out {
		buf.Write(v)
		buf.Write(space)
	}

	fmt.Println(buf.String())
}

func accumulateCounts(counts []int) []int {
	var total, acc = 0, make([]int, len(counts))

	for i := 0; i < len(acc); i++ {
		total += counts[i]
		acc[i] = total
	}

	return acc
}
