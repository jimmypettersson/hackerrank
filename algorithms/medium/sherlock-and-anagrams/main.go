package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var s string
		fmt.Scanln(&s)

		var count int
		for i := 1; i < len(s); i++ {
			var substrings = []string{}
			for j := 0; j+i <= len(s); j++ {
				for _, ss := range substrings {
					if isAnagram(ss, s[j:j+i]) {
						count++
					}
				}
				substrings = append(substrings, s[j:j+i])
			}
		}

		fmt.Println(count)
	}
}

func isAnagram(a, b string) bool {
	var ac, bc = make([]int, 26), make([]int, 26)

	for i := 0; i < len(a); i++ {
		ac[int(a[i])%97]++
		bc[int(b[i])%97]++
	}

	for i := 0; i < len(ac); i++ {
		if ac[i] != bc[i] {
			return false
		}
	}

	return true
}
