package main

import (
	"fmt"
	"strings"
)

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n, m, k int
		fmt.Scanln(&n, &m)

		var lines = make([]string, n)
		for r := 0; r < n; r++ {
			var s string
			fmt.Scan(&s)
			lines[r] = s
		}

		fmt.Scanln(&k)

		var waves int
		var forest, startRow, startCol, endRow, endCol = generateForest(lines, n, m)
		dfs(forest, &waves, startRow, startCol, endRow, endCol)

		if waves == k {
			fmt.Println("Impressed")
		} else {
			fmt.Println("Oops!")
		}
	}
}

func dfs(m [][]int, waves *int, startRow, startCol, endRow, endCol int) {
	// Mark this cell as visited
	m[startRow][startCol] = 4

	// Check if we've reached the goal
	if m[endRow][endCol] == 4 {
		return
	}

	var adjCells = adjacentCells(m, startRow, startCol)

	// Increase number of "waves" if we're at a crossroads
	if len(adjCells) > 1 {
		*waves++
	}

	/*
		for i := len(adjCells)-1; i >=0; i-- {
			var r, c = adjCells[i][0], adjCells[i][1]
			if m[r][c] == 0 || m[r][c] == 3 {
				dfs(m, waves, r, c, endRow, endCol)
			}
		}
	*/

	for _, cell := range adjCells {
		var r, c = cell[0], cell[1]
		if m[r][c] == 0 || m[r][c] == 3 {
			dfs(m, waves, r, c, endRow, endCol)
		}
	}

	// We reached a dead end
	if m[endRow][endCol] == 3 {
		// Decrease waves if we passed a crossroads
		if len(adjCells) > 1 {
			*waves--
		}
	}
}

func generateForest(lines []string, n, m int) ([][]int, int, int, int, int) {
	var forest = make([][]int, n)
	var startRow, startCol, endRow, endCol int

	for r := 0; r < n; r++ {
		forest[r] = make([]int, m)
		var chars = strings.Split(lines[r], "")
		for c := 0; c < m; c++ {
			switch chars[c] {
			case ".":
				forest[r][c] = 0
				break
			case "X":
				forest[r][c] = 1
				break
			case "M":
				forest[r][c] = 2
				startRow, startCol = r, c
				break
			case "*":
				forest[r][c] = 3
				endRow, endCol = r, c
				break
			}
		}
	}

	return forest, startRow, startCol, endRow, endCol
}

func adjacentCells(m [][]int, row, col int) [][]int {
	var adjCells = [][]int{}

	for i := -1; i < 2; i++ {
		for j := -1; j < 2; j++ {
			// Cant move diagonally
			if abs(i) == abs(j) {
				continue
			}
			// Out of bounds
			if row+i < 0 || row+i >= len(m) {
				continue
			}
			// Out of bounds
			if col+j < 0 || col+j >= len(m[0]) {
				continue
			}
			// The cell were currently on
			if row+i == row && col+j == col {
				continue
			}
			// Tree in the way
			if m[row+i][col+j] == 1 {
				continue
			}
			// We dont want to move back
			if m[row+i][col+j] == 4 {
				continue
			}
			adjCells = append(adjCells, []int{row + i, col + j})
		}
	}

	return adjCells
}

func abs(a int) int {
	if a < 0 {
		return -a
	}

	return a
}
