package main

import (
	"fmt"
	"sort"
)

var denominations []int
var memo map[string]int64

func main() {
	var n, m int
	fmt.Scanln(&n, &m)

	denominations = make([]int, m)
	for i := 0; i < m; i++ {
		var v int
		fmt.Scan(&v)
		denominations[i] = v
	}

	sort.Sort(sort.Reverse(sort.IntSlice(denominations)))
	memo = map[string]int64{}

	fmt.Println(countWays(n, 0))
}

func countWays(amount, pos int) int64 {
	if amount == 0 {
		return 1
	}

	if pos >= len(denominations) {
		return 0
	}

	var key = fmt.Sprintf("%d:%d", amount, pos)
	if n, ok := memo[key]; ok {
		return n
	}

	var numCoins int
	var ways int64
	for numCoins*denominations[pos] <= amount {
		var modAmount = amount - (numCoins * denominations[pos])
		var innerKey = fmt.Sprintf("%d:%d", modAmount, pos+1)
		if n, ok := memo[innerKey]; ok {
			ways += n
		} else {
			ways += countWays(modAmount, pos+1)
		}
		numCoins++
	}

	memo[key] = ways
	return ways
}
