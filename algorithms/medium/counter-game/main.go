package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n uint64
		fmt.Scanln(&n)

		if n == 1 {
			fmt.Println("Richard")
			continue
		}

		var richard bool
		for {
			if powerOfTwo(n) {
				n >>= 1
			} else {
				n -= nextPower(n)
			}

			if n == 1 {
				break
			}

			richard = !richard
		}

		if richard {
			fmt.Println("Richard")
		} else {
			fmt.Println("Louise")
		}
	}
}

func powerOfTwo(n uint64) bool {
	return n&(n-1) == 0
}

func nextPower(n uint64) uint64 {
	var e uint64
	for n > 0 {
		n >>= 1
		e++
	}

	return 1 << (e - 1)
}
