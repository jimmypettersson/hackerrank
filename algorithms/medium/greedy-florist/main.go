package main

import (
	"fmt"
	"sort"
)

func main() {
	var n, k int
	fmt.Scanln(&n, &k)

	var costs = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		costs[i] = v
	}

	sort.Ints(costs)

	var purchases = make([]int, k)
	var currentFriend, sum int

	for i := n - 1; i >= 0; i-- {
		var friend = currentFriend % k
		var x = purchases[friend]
		var cost = (x + 1) * costs[i]
		purchases[friend]++
		currentFriend++
		sum += cost
	}

	fmt.Println(sum)
}
