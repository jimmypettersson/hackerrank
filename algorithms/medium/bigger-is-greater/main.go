package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var b []byte
		fmt.Scanln(&b)

		var org = make([]byte, len(b))
		copy(org, b)

		var found bool
	L:
		for nextPermutation(b) {
			if bigger(b, org) {
				fmt.Println(string(b))
				found = true
				break L
			}
		}

		if !found {
			fmt.Println("no answer")
		}
	}
}

func nextPermutation(arr []byte) bool {
	var k, l int
	for k = len(arr) - 2; ; k-- {
		if k < 0 {
			return false
		}

		if arr[k] < arr[k+1] {
			break
		}
	}
	for l = len(arr) - 1; !(arr[k] < arr[l]); l-- {
	}
	arr[k], arr[l] = arr[l], arr[k]
	for i, j := k+1, len(arr)-1; i < j; i++ {
		arr[i], arr[j] = arr[j], arr[i]
		j--
	}
	return true
}

func bigger(a, b []byte) bool {
	for i := 0; i < len(a); i++ {
		if a[i] > b[i] {
			return true
		} else if a[i] < b[i] {
			return false
		}
	}

	return false
}
