package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	b, _ := ioutil.ReadAll(os.Stdin)
	lines := strings.Split(string(b), "\n")
	var numbers = strings.Fields(lines[1])

	n, _ := strconv.Atoi(strings.Fields(lines[0])[0])
	k, _ := strconv.Atoi(strings.Fields(lines[0])[1])

	var counts = map[int]struct{}{}
	var pairs int
	for i := 0; i < n; i++ {
		v, _ := strconv.Atoi(numbers[i])
		counts[v] = struct{}{}

		if _, ok := counts[v-k]; ok {
			pairs++
		}
		if _, ok := counts[v+k]; ok {
			pairs++
		}
	}

	fmt.Println(pairs)
}
