package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)

	var sorted, canSwap = true, false
	var l, r, outOfOrder int
	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		var v int
		fmt.Scan(&v)
		arr[i] = v

		// Swap logic
		if i > 0 && v < arr[i-1] {
			sorted = false
			outOfOrder++
			canSwap = outOfOrder < 3
			if outOfOrder == 1 {
				l, r = i-1, i
			} else {
				l, r = r-1, i
			}
		}
	}

	if sorted {
		fmt.Println("yes")
		return
	}

	if canSwap && isValidSwap(arr, l, r) {
		fmt.Println("yes")
		fmt.Println("swap", l+1, r+1)
		return
	}

	// Reverse logic
	var dl, dr, decendingSegments int
	for i := 0; i < len(arr)-1; i++ {
		if arr[i+1] < arr[i] {
			decendingSegments++
			if decendingSegments > 1 {
				break
			}

			dl, dr = i, i+1
			for j := i + 1; arr[j+1] < arr[j] && j < len(arr)-1; j++ {
				dr++
			}

			i = dr
		}
	}

	if decendingSegments == 1 && isValidSwap(arr, dl, dr) {
		fmt.Println("yes")
		fmt.Println("reverse", dl+1, dr+1)
		return
	}

	fmt.Println("no")
}

func isValidSwap(arr []int, l, r int) bool {
	return !(l-1 >= 0 && arr[r] < arr[l-1]) &&
		!(l+1 < len(arr) && arr[r] > arr[l+1]) &&
		!(r-1 >= 0 && arr[l] < arr[r-1]) &&
		!(r+1 < len(arr) && arr[l] > arr[r+1])
}
