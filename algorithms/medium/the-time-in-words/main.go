package main

import (
	"bytes"
	"fmt"
)

var n2w = map[int]string{
	1:  "one",
	2:  "two",
	3:  "three",
	4:  "four",
	5:  "five",
	6:  "six",
	7:  "seven",
	8:  "eight",
	9:  "nine",
	10: "ten",
	11: "eleven",
	12: "twelve",
	13: "thirteen",
	14: "fourteen",
	16: "sixteen",
	17: "seventeen",
	18: "eightteen",
	19: "nineteen",
	20: "twenty",
}

func main() {
	var h, m int
	fmt.Scanln(&h)
	fmt.Scanln(&m)

	var buf bytes.Buffer
	switch m {
	case 0:
		buf.WriteString(fmt.Sprintf("%s o' clock", n2w[h]))
		break
	case 15:
		buf.WriteString(fmt.Sprintf("quarter past %s", n2w[h]))
		break
	case 30:
		buf.WriteString(fmt.Sprintf("half past %s", n2w[h]))
		break
	case 45:
		buf.WriteString(fmt.Sprintf("quarter to %s", n2w[h+1]))
		break
	default:
		if m < 30 {
			buf.WriteString(fmt.Sprintf("%s %s %s %s", convertMin(m), plural(m), "past", n2w[h]))
		} else {
			buf.WriteString(fmt.Sprintf("%s %s %s %s", convertMin(m), plural(m), "to", n2w[h+1]))
		}
		break
	}

	fmt.Println(buf.String())
}

func convertMin(m int) string {
	if m == 15 {
		return "quarter"
	}

	if m > 20 && m < 30 {
		return n2w[20] + " " + n2w[m-20]
	}

	if m > 30 {
		return convertMin(60 - m)
	}

	return n2w[m]
}

func plural(m int) string {
	if m == 1 || m == 59 {
		return "minute"
	}

	return "minutes"
}
