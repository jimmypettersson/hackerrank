package main

import (
	"container/heap"
	"fmt"
	"math"
)

type Item struct {
	value    *node
	priority int
	index    int
}

type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].priority < pq[j].priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)
	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func (pq *PriorityQueue) update(item *Item, priority int) {
	item.priority = priority
	heap.Fix(pq, item.index)
}

type node struct {
	id       int
	adjacent map[*node]int
}

func (n *node) addEdge(other *node, r int) {
	// Only add same edge if its shorter
	if rr, ok := n.adjacent[other]; ok {
		if r < rr {
			n.adjacent[other] = r
			other.adjacent[n] = r
		}
	} else {
		n.adjacent[other] = r
		other.adjacent[n] = r
	}
}

func main() {
	var n, m, s int
	fmt.Scanln(&n, &m)

	var graph []*node
	for i := 0; i < n; i++ {
		graph = append(graph, &node{id: i, adjacent: map[*node]int{}})
	}

	for i := 0; i < m; i++ {
		var x, y, r int
		fmt.Scanln(&x, &y, &r)

		graph[x-1].addEdge(graph[y-1], r)
	}

	fmt.Scanln(&s)
	var sum = mst(graph, graph[s-1])
	fmt.Println(sum)
}

func mst(graph []*node, source *node) int {
	var pq = make(PriorityQueue, len(graph))
	var items = make([]*Item, len(graph))
	var C = make([]int, len(graph))
	var Q = make(map[*node]struct{})

	for _, v := range graph {
		C[v.id] = int(math.MaxInt32)
		Q[v] = struct{}{}
		pq[v.id] = &Item{v, C[v.id], v.id}
		items[v.id] = pq[v.id]
	}

	C[source.id] = 0
	pq[source.id].priority = 0
	heap.Init(&pq)

	for pq.Len() > 0 {
		var u = heap.Pop(&pq).(*Item).value
		delete(Q, u)

		for v, r := range u.adjacent {
			if _, ok := Q[v]; ok {
				if r < C[v.id] {
					C[v.id] = r
					pq.update(items[v.id], r)
				}
			}
		}
	}

	return sum(C)
}

func sum(n []int) int {
	var sum int
	for _, v := range n {
		sum += v
	}

	return sum
}
