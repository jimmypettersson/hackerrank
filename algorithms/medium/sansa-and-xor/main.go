package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n int
		fmt.Scanln(&n)

		var arr = make([]int, n)
		for i := 0; i < n; i++ {
			var v int
			fmt.Scan(&v)
			arr[i] = v
		}

		if len(arr)%2 == 0 {
			fmt.Println(0)
		} else {
			var r int
			for i := 0; i < len(arr); i = i + 2 {
				r ^= arr[i]
			}

			fmt.Println(r)
		}
	}
}
