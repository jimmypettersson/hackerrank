package main

import "fmt"

func main() {
	var rows, cols int
	fmt.Scanln(&rows)
	fmt.Scanln(&cols)

	var m = make([][]int, rows)
	for r := 0; r < rows; r++ {
		m[r] = make([]int, cols)
		for c := 0; c < cols; c++ {
			var v int
			fmt.Scan(&v)
			m[r][c] = v
		}
	}

	var max int
	// Try all starting positions
	for r := 0; r < len(m); r++ {
		for c := 0; c < len(m[0]); c++ {
			if m[r][c] == 1 {
				var visited = dfs(m, r, c)
				if visited > max {
					max = visited
				}
			}
		}
	}

	fmt.Println(max)
}

func dfs(m [][]int, row, col int) int {
	if m[row][col] == 0 {
		return 0
	}

	// Mark cell visited
	m[row][col] = 2

	var visited, adjCells = 0, adjacentCells(m, row, col)
	for _, cell := range adjCells {
		var r, c = cell[0], cell[1]
		if m[r][c] == 2 {
			continue
		}

		visited += dfs(m, r, c)
	}

	return 1 + visited
}

func adjacentCells(m [][]int, row, col int) [][]int {
	var adjCells = [][]int{}

	for i := -1; i < 2; i++ {
		for j := -1; j < 2; j++ {
			if row+i < 0 || row+i >= len(m) {
				continue
			}
			if col+j < 0 || col+j >= len(m[0]) {
				continue
			}
			if row+i == row && col+j == col {
				continue
			}
			if m[row+i][col+j] == 0 {
				continue
			}
			adjCells = append(adjCells, []int{row + i, col + j})
		}
	}

	return adjCells
}
