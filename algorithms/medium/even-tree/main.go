package main

import "fmt"

type node struct {
	id    int
	edges []*node
}

func newNode(id int) *node {
	return &node{
		id:    id,
		edges: []*node{},
	}
}

func (n *node) addEdge(other *node) {
	n.edges = append(n.edges, other)
	other.edges = append(other.edges, n)
}

func main() {
	var n, m int
	fmt.Scanln(&n, &m)

	var graph = make([]*node, n)
	for i := 0; i < m; i++ {
		var x, y int
		fmt.Scanln(&x, &y)

		if graph[x-1] == nil {
			graph[x-1] = newNode(x - 1)
		}

		if graph[y-1] == nil {
			graph[y-1] = newNode(y - 1)
		}

		graph[x-1].addEdge(graph[y-1])
	}

	var _, cuts = dfs(graph[0], nil, make([]bool, n))
	fmt.Println(cuts)
}

func dfs(n *node, p *node, visited []bool) (int, int) {
	if visited[n.id] {
		return 0, 0
	}

	visited[n.id] = true

	var vertices, cuts int
	for _, nn := range n.edges {
		var v, c = dfs(nn, n, visited)
		vertices += v
		cuts += c
	}

	vertices++

	if vertices%2 == 0 && p != nil {
		cuts++
	}

	return vertices, cuts
}
