package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	var b, _ = ioutil.ReadAll(os.Stdin)
	var lines = strings.Split(string(b), "\n")

	var arr, left, right = []int{}, []int{}, []int{}
	for _, line := range lines[1:] {
		var v, err = strconv.Atoi(strings.TrimSpace(line))
		if err != nil {
			continue
		}

		arr = append(arr, v)
		left = append(left, 1)
		right = append(right, 1)
	}

	// left pass
	for i := 1; i < len(arr); i++ {
		if arr[i-1] < arr[i] {
			left[i] = left[i-1] + 1
		}
	}

	// right pass
	for i := len(arr) - 2; i >= 0; i-- {
		if arr[i+1] < arr[i] {
			right[i] = right[i+1] + 1
		}
	}

	var sum int
	for i := 0; i < len(arr); i++ {
		sum += max(left[i], right[i])
	}

	fmt.Println(sum)
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
