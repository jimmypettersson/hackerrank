package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var a, b uint32
		fmt.Scan(&a)
		fmt.Scan(&b)

		if log2(a) != log2(b) {
			fmt.Println(0)
			continue
		}

		var r uint32
		var f bool
		for i := 31; i >= 0; i-- {
			var as, bs = isSet(a, uint(i)), isSet(b, uint(i))
			if as && bs {
				r = setBit(r, uint(i))
				f = true
			} else if f {
				if as == bs {
					if as {
						r = setBit(r, uint(i))
					}
				} else {
					break
				}
			}
		}

		fmt.Println(r)
	}
}

func isSet(n uint32, pos uint) bool {
	return n&(1<<pos) > 0
}

func setBit(n uint32, pos uint) uint32 {
	n |= (1 << pos)
	return n
}

func log2(n uint32) int {
	for i := 31; i >= 0; i-- {
		if isSet(n, uint(i)) {
			return i
		}
	}

	return 0
}
