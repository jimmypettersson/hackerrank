package main

import (
	"fmt"
	"strconv"
	"strings"
)

type node struct {
	distance int
	adjacent []*node
}

func (n *node) addEdge(other *node) {
	n.adjacent = append(n.adjacent, other)
	other.adjacent = append(other.adjacent, n)
}

func (n *node) String() string {
	return strconv.Itoa(n.distance)
}

type queue struct {
	buffer []*node
}

func (q *queue) enqueue(n *node) {
	q.buffer = append([]*node{n}, q.buffer...)
}

func (q *queue) dequeue() *node {
	var n = q.buffer[len(q.buffer)-1]
	q.buffer = q.buffer[:len(q.buffer)-1]
	return n
}

func (q *queue) empty() bool {
	return len(q.buffer) == 0
}

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n, m, s int
		fmt.Scanln(&n, &m)

		var graph []*node
		for i := 0; i < n; i++ {
			graph = append(graph, &node{adjacent: []*node{}})
		}

		for i := 0; i < m; i++ {
			var x, y int
			fmt.Scanln(&x, &y)

			graph[x-1].addEdge(graph[y-1])
		}

		fmt.Scanln(&s)
		var root = graph[s-1]
		graph = append(graph[:s-1], graph[s:]...)
		bfs(graph, root)
		fmt.Println(strings.Trim(fmt.Sprint(graph), "[]"))
	}
}

func bfs(graph []*node, root *node) {
	for _, n := range graph {
		n.distance = -1
	}

	root.distance = 0
	var q = &queue{[]*node{}}
	q.enqueue(root)

	var distances = []int{}
	for !q.empty() {
		var current = q.dequeue()
		distances = append(distances, current.distance)

		for _, n := range current.adjacent {
			if n.distance == -1 {
				n.distance = current.distance + 6
				q.enqueue(n)
			}
		}
	}
}
