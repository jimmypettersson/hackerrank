package main

import (
	"fmt"
	"math"
)

func main() {
	var input []byte
	fmt.Scanln(&input)

	var l = len(input)
	var floor = int(math.Floor(math.Sqrt(float64(l))))
	var ceil = int(math.Ceil(math.Sqrt(float64(l))))

	for i := floor; i <= ceil; i++ {
		for j := i; j <= ceil; j++ {
			if i*j >= l {
				for col := 0; col < j; col++ {
					for row := 0; row < i; row++ {
						var idx = col + row*j
						if idx < len(input) {
							fmt.Print(string(input[idx]))
						}
					}
					fmt.Print(" ")
				}

				fmt.Println()
				return
			}
		}
	}
}
