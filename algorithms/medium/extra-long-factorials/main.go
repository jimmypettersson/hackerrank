package main

import (
	"fmt"
	"math/big"
)

func main() {
	var n int64
	fmt.Scanln(&n)

	i := big.NewInt(0)
	i.MulRange(1, n)
	fmt.Println(i)
}
