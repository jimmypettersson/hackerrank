package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var n int
		fmt.Scanln(&n)

		var arr = make([]int64, n)
		for i := 0; i < n; i++ {
			var v int64
			fmt.Scan(&v)
			arr[i] = v
		}

		fmt.Println(calculateProfit(arr))
	}
}

func calculateProfit(prices []int64) int64 {
	var max, maxIndex = findMax(prices)
	if maxIndex == 0 {
		if len(prices) > 0 {
			return calculateProfit(prices[1:])
		}

		return 0
	}

	var profit int64
	for i := maxIndex; i >= 0; i-- {
		profit += (max - prices[i])
	}

	return profit + calculateProfit(prices[maxIndex+1:])
}

func findMax(arr []int64) (int64, int) {
	var max int64
	var maxIndex int

	for i := len(arr) - 1; i >= 0; i-- {
		if arr[i] > max {
			max = arr[i]
			maxIndex = i
		}
	}

	return max, maxIndex
}
