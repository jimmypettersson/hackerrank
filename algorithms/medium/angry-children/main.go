package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	var b, _ = ioutil.ReadAll(os.Stdin)
	var lines = strings.Split(string(b), "\n")

	var n, _ = strconv.Atoi(lines[0])
	var k, _ = strconv.Atoi(lines[1])

	var arr = make([]int, n)
	for i, line := range lines[2:] {
		if len(line) == 0 {
			continue
		}

		var v, _ = strconv.Atoi(line)
		arr[i] = v
	}

	sort.Ints(arr)

	var min = int(math.MaxInt32)
	for i := 0; i < n-k+1; i++ {
		var left = arr[i+k-1] - arr[i]
		if left < min {
			min = left
		}

		var right = arr[n-i-1] - arr[n-k-i]
		if right < min {
			min = right
		}

		if i >= n-k-i {
			break
		}
	}

	fmt.Println(min)
}
