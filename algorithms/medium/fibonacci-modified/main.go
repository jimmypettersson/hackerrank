package main

import (
	"fmt"
	"math/big"
)

var a, b int64

func main() {
	var n int64
	fmt.Scanln(&a, &b, &n)
	fmt.Println(fib(big.NewInt(n - 1)))
}

func fib(n *big.Int) *big.Int {
	if n.Int64() == 0 {
		return big.NewInt(a)
	}
	if n.Int64() == 1 {
		return big.NewInt(b)
	}

	var n1 = fib(big.NewInt(0).Sub(n, big.NewInt(1)))
	var n2 = fib(big.NewInt(0).Sub(n, big.NewInt(2)))
	return n1.Mul(n1, n1).Add(n1, n2)
}
