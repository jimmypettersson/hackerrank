package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"strings"
)

type node struct {
	id      int
	edges   []*node
	value   int
	treeSum int
}

func newNode(id, value int) *node {
	return &node{
		id:      id,
		edges:   []*node{},
		value:   value,
		treeSum: value,
	}
}

func (n *node) addEdge(other *node) {
	n.edges = append(n.edges, other)
	other.edges = append(other.edges, n)
}

func main() {
	b, _ := ioutil.ReadAll(os.Stdin)
	lines := strings.Split(string(b), "\n")
	values := strings.Fields(lines[1])

	var graph = []*node{}

	for i, v := range values {
		value, _ := strconv.Atoi(v)
		graph = append(graph, newNode(i, value))
	}

	edges := lines[2:]
	for _, v := range edges {
		if len(v) == 0 {
			continue
		}

		nodes := strings.Fields(v)
		from, _ := strconv.Atoi(nodes[0])
		to, _ := strconv.Atoi(nodes[1])
		graph[from-1].addEdge(graph[to-1])
	}

	fmt.Println(findMinDiff(graph))
}

func findMinDiff(graph []*node) int {
	var maxVal = dfs(graph[0], make([]bool, len(graph)))
	var n, min = len(graph), int(math.MaxInt64)

	for i := 1; i < n; i++ {
		var diff = abs(graph[i].treeSum - (maxVal - graph[i].treeSum))
		if diff < min {
			min = diff
		}
	}

	return min
}

func dfs(n *node, visited []bool) int {
	if visited[n.id] {
		return 0
	}

	visited[n.id] = true

	for _, nn := range n.edges {
		n.treeSum += dfs(nn, visited)
	}

	return n.treeSum
}

func abs(a int) int {
	if a < 0 {
		return -a
	}

	return a
}
