package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	s := bufio.NewScanner(os.Stdin)
	var buf bytes.Buffer
	for s.Scan() {
		buf.WriteString(strings.TrimSpace(s.Text()))
		buf.WriteString("\n")
	}

	var fakeFile = strings.NewReader(buf.String())

	var T int
	fmt.Fscanf(fakeFile, "%d\n", &T)

	for t := 0; t < T; t++ {
		var rows, cols int
		fmt.Fscanf(fakeFile, "%d %d\n", &rows, &cols)

		var matrix = make([][]int, rows)
		for i := 0; i < rows; i++ {
			matrix[i] = make([]int, cols)
		}

		for i := 0; i < rows; i++ {
			var row string
			fmt.Fscanf(fakeFile, "%s\n", &row)
			for j, v := range strings.Split(row, "") {
				vv, _ := strconv.Atoi(v)
				matrix[i][j] = vv
			}
		}

		var pRows, pCols int
		fmt.Fscanf(fakeFile, "%d %d\n", &pRows, &pCols)

		var pattern = make([][]int, pRows)
		for i := 0; i < pRows; i++ {
			pattern[i] = make([]int, pCols)
		}

		for i := 0; i < pRows; i++ {
			var row string
			fmt.Fscanf(fakeFile, "%s\n", &row)
			for j, v := range strings.Split(row, "") {
				vv, _ := strconv.Atoi(v)
				pattern[i][j] = vv
			}
		}

		if search(matrix, pattern) {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}

func search(m, p [][]int) bool {
	var mr, mc = len(m), len(m[0])
	var pc = len(p[0])

	for r := 0; r < mr; r++ {
		for c := 0; c < mc; c++ {
			if c+pc > mc {
				continue
			}

			if compare(m[r][c:c+pc], p[0]) {
				var found = true
				for i := range p {
					if !compare(m[r+i][c:c+pc], p[i]) {
						found = false
					}
				}

				if found {
					return true
				}
			}
		}
	}

	return false
}

func compare(m, p []int) bool {
	for i, e := range m {
		if e != p[i] {
			return false
		}
	}

	return true
}
